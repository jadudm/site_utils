import re, os
import shutil

calname = 'todo.ics'

inp = open('_site/%s' % calname, 'r')
out = open('_site/tmp.ics', 'w')

for line in inp:
  if re.search("DT", line):
    m = re.search("(.*?):([0-9]{4})([0-9]{2})([0-9]{2})([0-9]{2})([0-9]{2})", line)
    if m:
      line = "%s:%s%s%sT%s%s00\n" % \
        (m.group(1), m.group(2), m.group(3), m.group(4), m.group(5), m.group(6))
  
  # If there is stuff
  m = re.search('^\s*\n$', line)
  if not m:
    # print "Writing: %s" % line.strip()
    out.write(line.strip())
    out.write("\r\n")
    

out.close()
inp.close()

os.rename('_site/tmp.ics', '_site/%s' % calname)
